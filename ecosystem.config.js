module.exports = {
  apps : [
    {
      name: 'waterbird',
      script: 'index.js',
      cwd: '/opt/waterbird',
      instances: 1,
      kill_timeout: 180000,
      listen_timeout: 30000,
//      auto_restart: true,
      max_memory_restart: '3G',
      append_env_to_name: true,
      restart_delay: 15000,
      wait_ready: true,
      env_prd: {
        NODE_ENV: 'production',
        wb_wbenv: 'PRD'
      },
      env_prdt: {
        NODE_ENV: 'development',
        wb_wbenv: 'PRD',
        wb_autoresume: false
      },
      env_stg: {
        NODE_ENV: 'production',
        wb_wbenv: 'STG'
      },
      env_stgt: {
        NODE_ENV: 'development',
        wb_wbenv: 'STG',
        wb_autoresume: false
      },
      env_tst: {
        NODE_ENV: 'development',
        wb_wbenv: 'TST',
        wb_autoresume: false
      },
      env_dev: {
        NODE_ENV: 'development',
        wb_wbenv: 'DEV',
        wb_autoresume: false
      }
    }
  ]
};
