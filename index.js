#!/usr/bin/env node

const argv = require('yargs')
  .env('wb')
  .option('wbenv', {
    type: "string",
    describe: "The waterbird environment",
    choices: ['TST', 'STG', "PRD", "DEV"]
  })
  .option('mode', {
    type: "string",
    describe: "API request mode",
    choices: ['UPDATE', 'INSERT']
  })
  .option('headless', {
    type: 'boolean',
    alias: 'h',
    default: true
  })
  .option("type", {
    alias: 't',
    type: "string",
    describe: "The type of records to request from the API",
    choices: ['subject', 'case', 'specimen', 'migration']
  })
  .option('bots', {
    type: "number",
    alias: "b",
    describe: "Maximum number of bots to run (will not exceed credentials available)"
  })
  .option('bot_ids', {
    type: 'array',
    describe: "An array of bot ids to run (will ignore any bots about available credentials)"
  })
  .option('entity_id', {
    type: 'string',
    describe: "A specific entity to try to enter"
  })
  .option('debug', {
    type: 'boolean',
    describe: 'Run waterbird in debug mode',
    default: false
  })
  .option('slomo', {
    type: "number",
    describe: "Number of milliseconds to slow waterbird down at each step",
    default: 0
  })
  .option('autoresume', {
    type: 'boolean',
    describe: 'Should waterbird continue running after verify completes?',
    default: true
  })
  .demandOption('wbenv', "You must provide a wbenv argument for waterbird to run.")
  .help()
  .argv;

if(argv.entity_id !== undefined) {
  argv.debug = true;
}

if(argv.debug === true) {
  argv.bot_ids = 9;
  argv.headless = false;
}

if(argv.bot_ids !== undefined) {
  delete argv.bots;
}

const Controller = require("./lib/controller.js");
const path = require('path');
const debug = require('debug')('waterbird:index');

async function run() {
  debug("Starting waterbird controller");
  debug("Waterbird Options:");
  debug(argv);

//  await sendEmail('Testing', 'This is a test.');

//  console.log(argv);

//  debug(argv);
  const controller = new Controller(argv);
}

run();
