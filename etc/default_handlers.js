const InputHandler = require(global.wbDir + "/lib/wbInputHandler.js");

let handlers = {};
let handler_defs = [
  {
    name: 'text',
    input_fn: async function(step) {
      this.debug("text: " + step.xpath + ": " + step.value);
      try {
        let eh = await this.findElement(step, step.xpath);
        let tagName = await step.page.evaluate((tag) => {
          return tag.tagName;
        }, eh);
        if(tagName !== "INPUT") {
          eh = await eh.$("input");
        }
        if(step.config.overwrite) {
          await step.page.evaluate((el) => {
            el.value = '';
          }, eh);
        }
        await eh.type(step.value, {delay: 5});
      } catch(err) {
        await this.processError(err, step);
        return false;
      }
      return true;
    },
    options: {
      check_fn: async function(step) {
        this.debug("text: " + step.xpath + ": " + step.value);
        let eh;
        try {
          eh = await this.findElement(step, step.xpath);
          let tagName = await step.page.evaluate((tag) => {
            return tag.tagName;
          }, eh);
          if(tagName !== "INPUT") {
            eh = await eh.$("input");
          };
        } catch(err) {
          await this.processError(err, step);
          return false;
        }
        let val = await step.robot.page.evaluate(el => {return el.value;}, eh);
        return this.check(val, step.value, step.config.exact);
      },
    },
  },
  {
    name: "button",
    input_fn: async function(step) {
      this.debug("button: " + step.xpath + ": " + step.value);
      try {
        let eh = await this.findElement(step, step.xpath);
        await eh.click();
      } catch(err) {
        await this.processError(err, step);
        return false;
      }
      return true;
    },
    options: {
      valueRequired: false,
    },
  },
  {
    name: "checkbox",
    input_fn: async function(step) {
      this.debug("checkbox: " + step.xpath + ": " + step.value);
      let value = step.value;
      if(value === "Y") {
        value = true;
      } else if(value === "N") {
        value = false;
      }
      try {
        let eh = await this.findElement(step, step.xpath);
        let p = await eh.getProperty('checked');
        if(await p.jsonValue() !== value) {
          await eh.click();
        };
      } catch(err) {
        await this.processError(err, step);
        return false;
      }
      return true;
    },
    options: {
      check_fn: async function(step) {
        this.debug("checkbox: " + step.xpath + ": " + step.value);
        let value = step.value;
        if(value === "Y") {
          value = true;
        } else if(value === "N") {
          value = false;
        }
        let eh = await this.findElement(step, step.xpath);
        let checked = await eh.getProperty('checked');
        return this.check(await checked.jsonValue(), value, step.config.exact);
      },
    }
  },
  {
    name: 'select',
    input_fn: async function(step) {
      this.debug("select: " + step.xpath + ": " + step.value);
        try {
        let eh = await this.findElement(step, step.xpath);
        await eh.$$eval("option", (options, text, match_type) => {
          let event = new Event('change', {bubbles: true});
          let value = null;
          let el = options[0].parentElement;
          if(match_type === "value") {
            value = text;
          } else {
            for(let i = 0; i < options.length; i++) {
              let option = options[i];
              if(match_type === 'exact') {
                if(option.text === text) {
                  value = option.value;
                  break;
                }
              } else if(match_type === 'partial'){
                if(option.text.includes(text)) {
                  value = option.value;
                  break;
                }
              } else if(match_type === 'regex') {
                let re = new RegExp(text);
                if(re.test(option.text)) {
                  value = option.value;
                  break;
                }
              }
            }
          }
          if(value !== null) {
            el.value = value;
            event.simulated = true;
            el.dispatchEvent(event);
          }
        }, step.value, step.config.match_type);
      } catch(err) {
        await this.processError(err, step);
        return false;
      }
      return true;
    },
    options: {
      check_fn: async function(step) {
        this.debug("select: " + step.xpath + ": " + step.value);
        let eh = await this.findElement(step, step.xpath);
        let val;
        if(step.config.match_type === 'value') {
          val = await step.robot.page.evaluate(el => {
            return el.value;
          }, eh);
        } else {
          val = await step.robot.page.evaluate(el => {
            return el.options[el.selectedIndex].text;
          }, eh);
        }
        return this.check(val, step.value, step.config.match_type);
      },
    },
  },
  {
    name: "checkbox_group",
    input_fn: async function(step) {
        this.debug("checkbox_group: " + step.xpath + ": " + step.value);
        try {
          let eh = await step.page.$x(step.xpath);
          if(!Array.isArray(step.value)) {
            step.value = [step.value];
          }
          step.value = step.value.map(String);
          for(let i = 0; i < eh.length; i++) {
            let v = await eh[i].getProperty('value');
            v = await v.jsonValue();
            let shouldCheck = step.value.indexOf(String(v)) > -1;
            let isChecked = await eh[i].getProperty('checked');
            isChecked = await isChecked.jsonValue();
            if(isChecked !== shouldCheck) {
              await eh[i].click();
            }
          }
        } catch(err) {
          await this.processError(err, step);
          return false;
        }
        return true;
    },
  },
  {
    name: "radio_buttons",
    input_fn: async function(step) {
      this.debug("radio_buttons: " + step.xpath + ": " + step.value);
      try {
        let eh = await this.findElement(step, step.xpath);
        await step.page.evaluate((el, value) => {
          let event = new Event('click', {bubbles: true});
          el.checked = value;
          el.dispatchEvent(event);
        }, eh, value);
      } catch(err) {
        await this.processError(err, step);
        return false;
      }
      return true;
    },
  },
  {
    name: "textarea",
    input_fn: async function(step) {
      this.debug("text_area: " + step.xpath + ": " + step.value);
      try {
        let eh = await this.findElement(step, step.xpath);
        if(step.config.overwrite) {
          await step.page.evaluate((el) => {
            el.value = '';
          }, eh);
        }
        await eh.type(step.value, {delay: 5});
      } catch(err) {
        await this.processError(err, step);
        return false;
      }
      return true;
    }
  },
  {
    name: "script",
    input_fn: async function(step) {
      this.debug("script: " + step.value);
      try {
        await step.page.evaluate(step.value);
      } catch(err) {
        await this.processError(err, step);
        return false;
      }
      return true;
    },
    options: {
      xpathRequired: false,
    },
  },
  {
    name: "section",
    input_fn: async function(step) {
        this.debug("section: " + step.config.section);
        let run_section = false;
        // This could really be more robust...
        for(let i = 0; i < step.config.steps.length; i++) {
          let ss = step.config.steps[i];
          this.debug(ss);
          if(ss.has('value')) {
            run_section = true;
            this.debug("Found value to enter. Must run " + step.config.section);
            break;
          }
          if(ss.has('source')) {
            if(step.data.has(ss.source)) {
              run_section = true;
              this.debug("Found source to enter. Must run " + step.config.section);
              break;
            }
          }
        }
        if(run_section) {
          this.debug("Section " + step.config.section + " should run");
          for(i=0;i<step.config.steps.length;i++) {
            let step_config = step.config.steps[i],
                input_handler, attempts = 0, success = false;
            if(step_config.has('type')) {
              if(step.robot.module.inputHandlers.has(step_config.type)) {
                while(!success && attempts < 2) {
                  success = await this.subStep(step_config, step);
                  attempts++;
                }
                if(success) {
                  this.debug("Section Step Successful");
                } else {
                  this.debug("Section Step Failed");
                  return false;
                }
              } else {
                this.debug("Section Step has unregistered type");
                return false;
              }
            } else {
              this.debug("Section Step Missing `type`");
              return false;
            }
          }
          this.debug("Section Entry Successful");
          return true;
        } else {
          this.debug("Nothing to do. Skipping section " + step.config.section);
          return true;
        }
      },
    options: {
      xpathRequired: false,
      valueRequired: false,
    },
  }
];

for(let i = 0; i < handler_defs.length; i++) {
  let name = handler_defs[i].name;
  handlers[name] = new InputHandler(name, handler_defs[i].input_fn, handler_defs[i].options);
}

module.exports = handlers;
