const EventEmitter = require('events');
const fs = require('fs');
const yaml = require('js-yaml');
const wbError = require(global.wbDir + '/lib/wbError.js');

function setBoolean(val, def = true) {
  if(val === true) {
    return true;
  } else if(val === false) {
    return false;
  } else {
    return def;
  }
}

class wbInputHandlerConstructor {
  constructor(name, input_fn, config) {

    config = config || {};
    this.debug = require('debug')('waterbird' + process.waterbird.id + ':input:' + name);;
    this.name = name;
    this.input_fn = input_fn;
    this.check_fn = config.check_fn || function(step) {return true;};
    this.valueRequired = setBoolean(config.valueRequired);
    this.xpathRequired = setBoolean(config.xpathRequired);
    this.defaults = yaml.safeLoad(fs.readFileSync(global.wbDir + '/etc/config.yaml')).inputHandler;
    this.module = process.waterbird;
    this.log = process.waterbird.log;

    let iH = this;

    class inputHandler extends EventEmitter{

      constructor(step_data, robot) {
        super();
        this.step_data = step_data;
        this.robot = robot;
        this.inputHandler = iH;
        this.found = true;
        this.attempt_count = 0;
        this.result = "none";
        this.success = false;
        this.step = null;
        this.log_errors = null;
      }

      async execute() {
        this.step = await this.inputHandler.processStep(this.step_data, this.robot, this);
        if(this.step.shouldRun) {
          if(this.step.xpath !== null) {
            this.inputHandler.debug("Waiting for Element to begin data entry: " + this.step.xpath);
            try{
              await this.step.page.waitForXPath(this.step.xpath, {timeout: this.step.config.timeout});
            } catch(err) {
              this.inputHandler.debug("ELEMENT NOT FOUND: " + this.step.xpath);
              this.inputHandler.processError(err, this.step);
              this.inputHandler.debug(err);
              this.found = false;
            }
          }
          if(this.found) {
            while(this.attempt_count < this.step.config.max_attempts && !this.success) {
              this.attempt_count++;
              this.inputHandler.debug("Attempting Data Entry: " + this.attempt_count + "/" + this.step.config.max_attempts);
              // It was logging the same error serveral times. This conditional is meant to make it only log the last attempt.
              if(this.attempt_count == this.step.config.max_attempts) {
                this.step.config.log_errors = this.step.log_errors;
              } else {
                this.step.config.log_errors = false;
              }
              this.success = await this.inputHandler.dataEntry(this.step);
              if(this.success) {
                this.result = 'success'; // data entry ran successfully
              } else {
                this.result = 'failure'; // data entry encountered an error
              }
            }
          } else {
            this.result = 'not_found'; // the data entry element couldn't be found
          }
        } else {
          if(this.step.conditionMet === false) {
            this.result = 'not_needed'; // the condition wasn't met for the step to run
          } else {
            this.result = "not_run"; // the step should have run but was missing needed information
          }
        }
        let result = {result: this.result, step: this.step, inputHandler: this.inputHandler};
        this.emit('finished', result);
        return result;

      };
    }
    return inputHandler;
  }

  async findElement(step, x, single_element = true) {
    let eh = await step.page.$x(x);
    if(Array.isArray(eh)) {
      if(single_element) {
        eh = eh[0];
      }
    }
    return eh;
  }

  async confirmElement(step, x) {
    let eh = await this.findElement(step, x);
    return eh !== null;
  }

  async subStep(substep, step, full_result = false) {
    if(this.module.inputHandlers.has(substep.type)) {
      try {
        let subInputHandler = new this.module.inputHandlers[substep.type](substep, step.robot, step.handler);
        let result = await subInputHandler.execute();
        if(full_result) {
          return result;
        } else {
          if(result.result === 'success' || result.result === 'na' || result.result === 'not_needed' || result.result === 'not_run') {
            return true;
          } else {
            return false;
          }
        }
      } catch(err) {
        await this.processError(err, step);
        return false;
      }
    } else {
      await this.processError("No inputHandler for substep: " + substep.type, step);
      return false;
    }
  }

  async processError(msg, step) {
    if(step.config.log_errors) {
      let error = new wbError(msg, step.robot, 'input_controller_' + this.name);
      await error.log();
    }
    if(msg instanceof Error) {
      this.debug(msg.message);
    } else {
      this.debug(msg);
    }
  }

  check(a, b, match_type = 'exact') {
    let exact;
    if(match_type === 'exact' || match_type === 'value') {
      exact = true;
    } else if(match_type == 'partial') {
      exact = false;
    }
    if(match_type === 'regex') {
      this.debug("new RegExp('" + b + "').test('" + a + "')");
      let re = new RegExp(b);
      return re.test(a);
    } else if(exact) {
      this.debug(a + " === " + b + " = ", (a === b));
      return a === b;
    } else {
      this.debug(a + ".includes(" + b  + ") = " + a.includes(b));
      return a.includes(b);
    }
  }

  async processStep(step_config, robot, handler) {

    let step = {
      robot: robot,
      page: robot.page,
      config: Object.assign({}, this.defaults, step_config),
      data: robot.data.data,
      handler: handler,
    };

    step.xpath = this.buildXPath(step);
    step.value = await this.buildValue(step);
    step.conditionMet = true;

    step.log_errors = step.config.log_errors; // Saving a backup of the original state. Since it tries several times only want to log the error on final attempt.
    step.shouldRun = await this.stepShouldRun(step);
    if(step.shouldRun) {
      this.handleDialogs(step);
    }

    return step;

  }

  handleDialogs(step) {
    step.page.removeAllListeners('dialog');
    step.page.on('dialog', async (dialog) => {
      this.debug("Handling Dialog");
      let handled = false;
      if(step.config.has('dialogs')) {
        this.debug("Applying Custom Dialog Handler");
        let dialogs = step.config.dialogs;
        if(Array.isArray(dialogs)) {
          for(let i = 0; i < dialogs.length; i++) {
            let match = true;
            let d = dialogs[i];
            if(d.has('action')) {
              if(d.has('type')) {
                if(d.type !== dialog.type()) {
                  match = false;
                }
              }
              if(d.has('message')) {
                if(dialog.message().includes(d.message) == false) {
                  match = false;
                }
              } else if(d.has('regex')) {
                let re = new RegExp(d.regex);
                if(!re.test(dialog.message())) {
                  match = false;
                }
              } else {
                match = true;
              }
              if(match) {
                if(d.action === 'accept') {
                  if(d.has('text')) {
                    await dialog.accept(d.text);
                  } else {
                    await dialog.accept();
                  };
                  handled = true;
                } else if(d.action === 'dismiss') {
                  await dialog.dismiss();
                  handled = true;
                }
              }
            }
          }
        } else {
          await this.processError("dialogs must be an array", step);
        }
      }
      if(!handled) {
        this.debug("Unhandled dialog");
        await dialog.dismiss();
        await this.processError("DIALOG: " + dialog.message(), step);
        step.handler.emit("finished", {result: "failure", step: step, inputHandler: this});
      }
    });
  }

  buildXPath(step, data) {
      this.debug("Building XPath");
      let xpath = null;
      let config;
      if(typeof step === 'object') {
        if(step.has('config')) {
          config = step.config;
        } else {
          config = step;
        }
        if(step.has('data')) {
          data = data || step.data;
        }
        if(config.has('id')) {
          xpath = "//*[@id='" + config.id + "']";
        } else if(config.has('name')) {
          xpath = "//*[@name='" + config.name + "']";
        } else if(config.has('xpath')) {
          xpath = config.xpath;
        }
      } else if(typeof step === 'string') {
        xpath = step;
      }
      if(xpath !== null) {
        let re = /<(.+)>/;
        if(re.test(xpath)) {
          let v = data[xpath.match(re)[1]];
          xpath = xpath.replace(/<.+>/, v);
        }
      }
      this.debug("XPath: " + xpath);
      return xpath;
  }

  async buildValue(step) {
      this.debug("Building Value");
      let value = null;
      if(step.config.has('value')) {
        this.debug("Applying Default Value: " + step.config.value);
        value = step.config.value;
      }
      if(step.config.has('source')) {
        this.debug("Looking up data source: " + step.config.source);
        if(step.data.has(step.config.source)) {
          this.debug("Source Data Found");
          if(value !== null) {
            value = value + "; " + step.data[step.config.source];
          } else {
            value = step.data[step.config.source];
          }
          step.data.used_source = step.data[step.config.source];
        } else if(step.config.has('alt_source')) {
          if(step.data.has(step.config.alt_source)) {
            this.debug("Alt Source Data Found");
            if(value !== null) {
              value = value + "; " + step.data[step.config.alt_source];
            } else {
              value = step.data[step.config.alt_source];
            }
            step.data.used_source = step.data[step.config.alt_source];
          }
        }
      }
      if(value !== null) {
        if(step.config.has('code')) {
          this.debug("Recoding Step Value");
          if(step.config.code.has(value)) {
            value = step.config.code[value];
          }
        }
        if(step.config.has('modifier')) {
          value = step.config.modifier.replace('<VALUE>', value);
        }
      }
      if(value === null) {
        this.debug("No Value Found");
      } else {
        this.debug("Value Found: " + value);
      }
      return value;
  }

  async stepShouldRun(step) {
    this.debug("Checking if step should run");
    let run = true;
    if(this.valueRequired && step.value === null) {
      this.debug("Required Value Not Found");
      run = false;
    } else if(this.xpathRequired && step.xpath === null) {
      this.debug("Required XPath Not Found");
      run = false;
    } else if(step.config.has("condition")) {
      this.debug("Checking Special Step Condition: ");
      this.debug(step.config.condition);
      let check;
      let xpath = this.buildXPath(step.config.condition, step.data);
      try{
        check = await this.findElement(step, xpath);
      } catch(err) {
        this.debug("STEP SHOULD RUN ERROR");
        await this.processError(err, step);
        run = false;
      }
      step.conditionMet = (check !== undefined) === step.config.condition.exists;
      this.debug("conditionMet:" + step.conditionMet);
      run = step.conditionMet;
    }
    this.debug("STEP SHOULD RUN: " + run);
    return run;
  }

  async processWaits(step) {
    if(step.config.has('wait')) {
      this.debug("Processing Step Waits");
      try {
        if(step.config.wait === 'navigation') {
          this.debug("Waiting for navigation");
          await step.page.waitForNavigation();
          this.debug("Navigation Complete");
        } else if(typeof step.config.wait === 'number') {
          this.debug("Waiting for " + (step.config.wait / 1000) + " seconds");
          let x = await step.page.waitFor(step.config.wait);
        } else {
          let xpath = this.buildXPath(step.config.wait, step.data);
          if(xpath !== null) {
            await step.page.waitForXPath(xpath);
          }
        }
        return true;
      } catch(err) {
        this.debug("Error Processing Step Wait...");
        await this.processError(err, step);
        return false;
      }
    }
    return true;
  }

  async dataEntry(step) {
    let result;
    try {
      if(await this.input_fn.call(this, step)) {
        if(await this.processWaits(step)) {
          if(step.config.verify) {
            result = await this.check_fn.call(this, step);
          } else {
            result = true;
          }
        } else {
          result = false;
        }
      } else {
        result = false;
      }
    } catch(err) {
      this.debug("STEP DATA ENTRY ERROR");
      await this.processError(err, step);
      result = false;
    } finally {
      return result;
    }


  }

}

module.exports = wbInputHandlerConstructor;
