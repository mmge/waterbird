var debug, debug_timing;
const EventEmitter = require('events');
const yaml = require('js-yaml');
const fs = require('fs');
const wbError = require(global.wbDir + '/lib/wbError.js');

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

class wbRobot extends EventEmitter { // Coresponds to a page in a browser.
  constructor(module) {
    super();
    let robot = this;
    debug = require('debug')('waterbird' + module.id + ':robot');
    debug_timing = require('debug')('waterbird' + module.id + ':timing');
    this.start_time = new Date();
    this.last_step_time = new Date();
    this.config = yaml.safeLoad(fs.readFileSync(global.wbDir + '/etc/config.yaml')).robot;
    this.module = module;
    debug(this.module.options);
    this.script = null;
    this.steps = [];
    this.page = null;
    this.data = {};
    this.request;
    this.stepNo = -1;
    this.attempts = 0;
    this.failedEntries = [];
    this.counts = {total: 0, attempted: 0, successful: 0};
    this.log = module.log;
    this.identifier;
    this.start_wait = this.config.api_wait*60*1000;
    this.max_wait = this.config.api_maxwait*60*1000;
    this.wait = this.start_wait;
    if(this.module.options.debug === false) {
      this.suicide_switch = setTimeout(async function() {
        try{
          let e = new wbError("robot lifespan exceeded", robot, "robot_timeout");
          robot.page.removeAllListeners('close');
          robot.report('failure', 'robot_timeout');
        } catch(err) {
          debug(err);
          let error = new wbError(err, robot, "robot_timeout");
          await error.log();
        }
      }, this.config.lifespan * 60 * 1000);
    }
  }

  // Convienience function for setting a sleep timer
  updateWaitTime(reset = false) {
    if(reset) {
      this.wait = this.start_wait;
    } else {
      this.wait = Math.round((Math.min(this.wait*2, this.max_wait) * (Math.random()*0.2 + 0.9)) / 1000) * 1000;
    }
  }

  report(result, msg) {
    debug("Robot Finished: ", result);
    debug(msg);
    this.emit('finished', result, msg);
    clearTimeout(this.suicide_switch);
  }

  async execute() {
    await this.getAPIData();
  }

  // Loops through the data and verifies that all required steps have values
  // in the dataset.
  verifyAPIData() {
    let data = this.data.data;
    let steps = this.steps;
    let input_defaults = yaml.safeLoad(fs.readFileSync(global.wbDir + '/etc/config.yaml')).inputHandler;
    let verifyData = function(steps, data) {
      for(let i = 0; i < steps.length; i++) {
        let step = steps[i];
        if(step.type === 'section') {
          let verify = verifyData(step.steps, data);
          if(verify !== 'verified') {
            return verify;
          }
        } else {
          step = Object.assign({}, input_defaults, step);
          if(step.required && step.has('source')) {
            if(!data.has(step.source)) {
              debug("Missing Source Field: " + step.source);
              if(step.has('alt_source')) {
                if(!data.has(step.alt_source)) {
                  debug("Missing Alt_Source Field: " + step.alt_source);
                  return step.alt_source;
                }
              } else {
                return step.source;
              }
            }
          }
        }
      }
      return 'verified';
    };

    return verifyData(steps, data);

  }

  // Requests data from the API, if received, sets properties, verifies the
  // data has required fields, and starts the page creation process.
  async getAPIData() {
    debug("Getting API Data");
    try {
      var data = await this.module.source.getData();
    } catch(err) {
      let error = new wbError(err, this, 'get_api_data');
      await error.log();
      debug("ERROR GETTING API DATA");
      debug(err);
    }
    debug(data);
    let result;
    try {
      if(data.mode === "error") {
        result = 'Bad Data';
      } else if((Object.keys(data).length === 0 && data.constructor === Object)) {
        result = 'No Data';
      } else if(data.length === 0) {
        result = "No Data";
      } else if(data.has('mode') && data.has('type') && data.has('data')) {
        result = 'Data Received';
      } else {
        result = 'Bad Data';
      }
    } catch(err) {
      debug(err);
      let error = new wbError(err, this, 'get_api_data');
      await error.log();
      result = 'Bad Data';
    }

    if(result === 'Data Received') {
      this.data = data;
      let script_loc = global.wbDir + "/scripts/" + data.mode + "_" + data.type + ".yaml";
      this.script = yaml.safeLoad(fs.readFileSync(script_loc));
      this.url = this.script.url;
      this.name = this.script.name;
      this.steps = this.script.steps;
      this.identifier = this.data.data[this.script.identifier];
      let verify = this.verifyAPIData();
      if(verify === 'verified') {
        debug("API Data Verified");
        await this.createPage();
      } else {
        debug("Data Missing Required fields...");
        this.report('data_error', {type: 'Missing Data', field: verify});
      }
    } else  {
      debug(result + " Returned by API");
      this.report('data_error', {type: result});
    }

  }

  // creates the page that the robot uses to do data entry. Sets various
  // event listeners to ensure that errors and dialogs are handled. Also
  // increments the attempts counter each time createPage is called. If
  // `attempts` exceeds `page_tries` will abort page creation and report
  // a data entry error to waterbird.
  async createPage() {
    debug("Creating Page for data entry");
    this.attempts++;
    this.counts = {total: 0, attempted: 0, successful: 0};
    if(this.attempts <= this.config.page_tries) {
      let success = false;
      try{
        if(this.page !== null) {
          try{
            await this.page.removeAllListeners('close');
            await this.page.close();
            this.page = null;
          } catch(err) {}
        }
        this.page = await this.module.browser.newPage();
        this.page.setDefaultNavigationTimeout(60000);
        this.page.on('close', async () => {
          let error = new wbError("Page closed before data entry completion", 'page_close');
          await error.log();
          debug("Page closed before data entry completion");
          await this.createPage();
        });
        this.page.on("error", async (err) => {
          let error = new wbError(err, this, 'page_error');
          await error.log();
          debug(err);
          try{
            if(this.module.options.debug === false) {
              await this.page.close();
            }
          } catch(err) {
            let error = new wbError(err, this, 'page_error_close_error');
            await error.log();
            debug("Page Close on Error ERROR");
            debug(err);
            await this.createPage();
          } finally {
            this.report('error', "Data entry failed: " + (this.current_step_config.id || this.current_step_config.name || this.current_step_config.xpath || this.current_step_config.type));
          }
        });
        this.page.on('pageerror', async (err) => {
          let error = new wbError(err, this, 'page_pageerror');
          await error.log();
          debug("pageerror");
          debug(err);
          if(this.module.options.debug === false) {
            try{
              this.page.removeAllListeners('close');
              await this.page.close();
            } catch(err) {
              let error = new wbError(err, this, 'page_pageerror_close_error');
              await error.log();
              debug("Page Close on pageerror ERROR");
              debug(err);
            } finally {
              this.report('page_error', "Data entry failed: " + (this.current_step_config.id || this.current_step_config.name || this.current_step_config.xpath || this.current_step_config.type));
            }
          }
        });
        this.page.on('dialog', async (dialog) => {
          debug("Unhandled dialog");
          await dialog.dismiss();
          let error = new wbError("DIALOG: " + dialog.message(), this, 'unhandled_dialog');
          await error.log();
          this.report('failure', "Data entry failed: " + (this.current_step_config.id || this.current_step_config.name || this.current_step_config.xpath || this.current_step_config.type));
          await this.page.close();
        });
        await this.page.setViewport({'width': this.module.options.width, 'height': this.module.options.height});
        await this.page.goto(this.module.urls.oncore + this.url);
        await this.page.waitForSelector("#user-name");
        this.page.robot = this;
        this.updateWaitTime(true);
        success = true;
        debug("Page Created");
      } catch(err) {
        let error = new wbError(err, this, 'page_creation_error');
        await error.log();
        debug("PAGE CREATION FAILED");
        debug(err);
        await this.module.sleep(this.wait, "Failed to create page");
        this.updateWaitTime();
        await this.createPage();
        return;
      }
      if(success) {
        this.stepNo = 0;
        await this.runStep();
      }
    } else {
      try {
        this.page.removeAllListeners('close');
      } catch(e) {}
      this.report('failure', "Too Many Page Creation Attempts");
    }
  }

  describeStep(result) {

    let s = this.steps[this.stepNo];
    let element_type = s.type;
    let page_element = s.name || s.id || s.xpath || "Unknown";
    let source_value = s.source || s.value || "Unknown";
    let display = true;

    if(result !== undefined) {
      display = !(result.result == 'not_run' || result.result == 'not_needed');
    }

    if(display === true) {
      debug_timing("------------------------------");
      debug_timing("Waterbird ID: " + this.module.id);
      debug_timing("Element Type: " + element_type);
      debug_timing("Page Element: " + page_element);
      debug_timing("Source Value: " + source_value);

      if(result !== undefined) {
        debug_timing("Step Result: " + result.result);
      }

      let current_time = new Date();
      let elapsed_time = Math.round((current_time - this.last_step_time)/1000, 1);
      this.last_step_time = current_time;
      debug_timing("Elapsed Time: " + elapsed_time + "s");
      debug_timing("------------------------------");
    }

  }

  // Runs an individual step. If it succeeds, increments the stepNo counter and
  // calls itself to run the next step, if there isn't a next step reports a
  // successful data entry to waterbird. errors are handled by either closing
  // the page and triggering another attempt or if `page_tries` has been
  // exceeded, aborts and reports a data entry error to waterbird
  async runStep() {
    debug("Calling Next Step");
    this.counts.total++;
    let step_config = Object.assign({}, this.steps[this.stepNo]), //BEWARE: Only a shallow copy. May cause problems. If it does try JSON.parse(JSON.stringify())
        input_handler,
        step_attempts = 0,
        result,
        keepGoing = true,
        elapsed_time,
        current_time;
    debug(step_config);
    this.current_step_config = step_config;
    this.counts.total++;
    if(step_config.has('type')) {
      if(this.module.inputHandlers.has(step_config.type)) {
        if(this.attempts <= this.config.page_tries) {
          step_config.log_errors = false;
        }
        input_handler = new this.module.inputHandlers[step_config.type](step_config, this);
        input_handler.on('finished', async (result) => {
          debug("Step Result: " + result.result);
          this.describeStep(result);
          switch(result.result) {
            case "success":
              this.counts.successful++;
              this.counts.attempted++;
              break;
            case "failure":
              this.counts.attempted++;
              if(result.step.config.required) {
                keepGoing = false;
              }
              break;
            case "not_found":
              if(result.step.config.required) {
                keepGoing = false;
              }
              break;
            case "not_needed":
              break;
            case "not_run":
              if(result.step.config.required) {
                keepGoing = false;
              }
              break;
          }

          if(keepGoing) {
            this.stepNo++;
            if(this.stepNo >= this.steps.length) {
              debug("Record Entry Complete");
	      debug(this.data);
              let total_elapsed = Math.round((new Date() - this.start_time)/1000, 1);
              debug_timing("Total Elapsed Time: " + total_elapsed + "s");
              this.page.removeAllListeners('close');
              this.report('success', {counts: this.counts, failed: this.failedEntries});
            } else {
              await this.runStep();
            }
          } else {
            if(this.attempts < this.config.page_tries) {
              this.page.removeAllListeners('close');
              await this.page.close();
              await this.createPage();
            } else {
              let error = new wbError('Required Step Result: ' + result.result + " - " + result.step.config.type + ":" + (result.step.config.value || result.step.config.source || ""), result.step.robot, 'required_step');
              await error.log();
              this.page.removeAllListeners('close');
              this.report('failure', "Data entry failed: " + (step_config.id || step_config.name || step_config.xpath || step_config.type));
            }
          }
        });
        await input_handler.execute();

      } else {
        this.page.removeAllListeners('close');
        this.report('failure', 'No inputHandler registered for ' + step_config.type);
      }
    }
  }

}

module.exports = wbRobot;
