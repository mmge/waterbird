const winston = require('winston');
require('winston-daily-rotate-file');
const fs = require('fs');
const yaml = require('js-yaml');
const {createLogger, format, transports} = require('winston');
const {combine, timestamp, printf} = format;
const shortid = require('shortid');
const wbDatabaseTransport = require(global.wbDir + "/lib/wbDatabaseTransport.js");

var debug;

class wbLogger{

  constructor(wb) {
    debug = require('debug')('waterbird' + process.waterbird.id + ':wbLogger');
    this.waterbird = wb;
    let d = new Date();
    this.date = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
    this.config = yaml.safeLoad(fs.readFileSync(global.wbDir + '/etc/config.yaml')).logs;
    let db_creds = yaml.safeLoad(fs.readFileSync(global.wbDir + '/etc/creds.yaml')).log_database;
    this.log_dir = this.config.dir;
    this.db_config = {
      user: db_creds.user,
      password: db_creds.password,
      server: this.config.database.server,
      database: this.config.database.database,
      pool: this.config.database.pool
    };
    if(!fs.existsSync(this.log_dir + "screenshots")) {
      fs.mkdirSync(this.log_dir + "screenshots");
    }
    this.screenshot_folder = this.log_dir + "screenshots/" + this.date + "/";
    if(!fs.existsSync(this.screenshot_folder)) {
      fs.mkdirSync(this.screenshot_folder);
    }

    this.data_log_format = winston.format.combine(
      winston.format.timestamp(),
      winston.format.printf(info => {
        let log = {timestamp: info.timestamp};
        let message = info.message;
        if(message.page !== null && message.page !== undefined) {
          let sid = shortid.generate();
          log.sid = sid;

          try {
            debug("Taking Screenshot: " + sid + ".png");
            message.page.screenshot({path: this.screenshot_folder + sid + ".png", fullPage: true, type: 'jpeg', quality: 25});
          } catch(err) {
            debug("Failed to take screenshot...");
            debug(err);
          }

          delete message.page;
          return JSON.stringify(Object.assign(info.message, log));
        } else {
          return JSON.stringify(Object.assign(info.message, log));
        }
      })
    );

    this.log_format = winston.format.combine(
      winston.format.timestamp(),
      winston.format.printf(info => {
        let log = {'level': info.level,
                   'env': info.wbenv,
                   'wbid': info.wbid,
                   'message': info.message,
                   'user': info.user,
                   'sid': "",
                   'timestamp': info.timestamp};
        return JSON.stringify(log);
      }
    ));
    this.transports = {
      data_entry: new (winston.transports.DailyRotateFile)({
        level: 'entry',
        filename: this.log_dir + 'data_entry-' + this.waterbird.environment + '-%DATE%.log',
        datePattern: "YYYY-MM-DD",
        zippedArchive: false,
        maxSize: '50m',
        maxFiles: '30d'
      }),
      application: new (winston.transports.DailyRotateFile)({
        level: 'info',
        filename: this.log_dir + 'waterbird-' + this.waterbird.environment + '-%DATE%.log',
        zippedArchive: false,
        maxSize: '50m',
        maxFiles: '30d'
      }),
      exceptions: new (winston.transports.DailyRotateFile)({
        filename: this.log_dir + 'exception-' + this.waterbird.environment + '-%DATE%.log',
        zippedArchive: false,
        maxSize: '50m',
        maxFiles: '30d'
      }),
    },
    this.loggers = {
      data: createLogger({
        levels: {
          entry: 0,
        },
        format: this.data_log_format,
        transports: [
          this.transports.data_entry,
        ],
        exitOnError: false
      }),
      app: createLogger({
        format: this.log_format,
        transports: [
          this.transports.application,
        ],
        exceptionHandlers: [
          this.transports.exceptions
        ],
        exitOnError: false
      }),
    };
  }
  data(robot, result, msg) {
    debug("Data Entry: " + result);
    let log_data = {
      result: result,
      wbid: this.waterbird.id,
      env: this.waterbird.environment,
      mode: robot.data.mode || "NA",
      type: robot.data.type || "NA",
      identifier: robot.identifier || "NA",
      user: this.waterbird.user,
      elapsed: (Math.round((new Date() - robot.start_time)/1000*10))/10
    };
    if(result === "failure") {
      log_data.step = robot.stepNo;
      log_data.page = robot.page;
      log_data.reason =  msg;
    } else if(result === "success") {
      log_data.rate = msg.counts.successful + "/" + msg.counts.attempted;
      if(msg.counts.successful !== msg.counts.attempted) {
        log_data.failed_entries = msg.failed.toString();
      }
    } else if(result === "data_error") {
      log_data.error = msg.type;
      log_data.missing_field = msg.field;
    }
    process.send({'data_entry': result});
    this.loggers.data.entry(log_data);
  }

  app(level, msg, page = null) {
    if(level !== 'error') {
      debug(msg);
    }
    let obj = {
      wbid: this.waterbird.id,
      user: this.waterbird.user,
      env: this.waterbird.environment,
      message: msg
    };
    this.loggers.app[level](obj);
  }
}

module.exports = wbLogger;
