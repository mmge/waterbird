const mssql = require("mssql");
const path = require('path');
const shortid = require('shortid');
const fs = require('fs');
const yaml = require('js-yaml');
const Transport = require('winston-transport');
const util = require('util');
var debug;

class wbDatabaseTransport extends Transport{

  constructor(opts) {
    debug = require('debug')('waterbird:dbLog');
    super(opts);
    let that = this;
    this.con_config = {
      user: opts.user,
      password: opts.password,
      server: opts.server,
      database: opts.database,
      pool: opts.pool
    };
    this.pool = "";
  }

  async get_pool() {

    console.log(typeof this.pool);
    if(this.pool === "") {
      console.log("Building connection...");
      try{
        this.pool = await mssql.connect(this.con_config);
        console.log("Connected to mssql for logging");
      } catch(err) {
        console.log(err);
      }
    }
    return this.pool;

  };

  async request(query) {
    let p = await this.get_pool();
    try {
      const result = await p.query(query);
      return result;
    } catch(err) {
      console.log(err);
    }
  }

  make_entity_query(data, fields) {

    let f = [];
    let v = [];

    for(let i = 0; i < fields.length; i++) {
      if(Object.keys(data).indexOf(fields[i]) > -1) {
        f.push(fields[i]);
        v.push("'" + data[fields[i]] + "'");
      }
    }

    f.push('timestamp');
    v.push('getdate()');

    let f_str = f.join(",");
    let v_str = v.join(",");
    let q = `INSERT INTO dbo.entities (${f_str}) VALUES (${v_str})`;

    return q;

  }

  make_values_query(data, fields) {

    let n = [];
    let entity_id = data.entity_id;
    let keys = Object.keys(data);

    for(let i = 0; i < keys.length; i++) {
      if(fields.indexOf(keys[i]) == -1) {
        n.push(`('${entity_id}','${keys[i]}','${data[keys[i]]}')`);
      }
    }
    let values = n.join(",");

    let q = `INSERT INTO dbo.log_values (entity_id, name, value) VALUES ${values};`;

    return q;

  }

  async close() {
    await this.pool.close();
  }

  async log(log_data, callback) {

    log_data.entity_id = shortid.generate();
    let entity_fields = ["entity_id", "entity_type", "oncore_env", "bot_id", "oncore_login"];
    let entity_query = this.make_entity_query(log_data, entity_fields);
    let values_query = this.make_values_query(log_data, entity_fields);
    console.log(await this.request(entity_query));
    console.log(await this.request(values_query));

    callback();

  }

}

module.exports = wbDatabaseTransport;
