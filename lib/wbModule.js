var debug;

const puppeteer = require('puppeteer');
const fs = require('fs');
const yaml = require('js-yaml');
const EventEmitter = require('events');
const path = require('path');
global.wbDir = path.resolve(__dirname + "/..");

const Robot = require(global.wbDir + "/lib/wbRobot.js");
const Request = require(global.wbDir + "/lib/wbDataRequest.js");
const InputHandler = require(global.wbDir + "/lib/wbInputHandler.js");
const wbLogger = require(global.wbDir + '/lib/wbLogger.js');
const wbError = require(global.wbDir + '/lib/wbError.js');

Object.prototype.has = function(key) {
  return Object.keys(this).indexOf(key) > -1;
};

class wbModule extends EventEmitter {
  constructor(options) {
    super();
    this.start_time = new Date();
    this.config = yaml.safeLoad(fs.readFileSync(global.wbDir + '/etc/config.yaml'));
    this.options = Object.assign({}, this.config.defaults, options);
    this.options.headless = this.options.headless === 'true';
    this.options.debug = this.options.debug === 'true';
    debug = require('debug')('waterbird' + this.options.id + ':waterbird');
    debug("Creating Waterbird Object");
    debug(this.options);
    process.waterbird = this;
    this.browser = null;
    this.id = options.id;
    this.credentials = yaml.safeLoad(fs.readFileSync(global.wbDir + '/etc/creds.yaml')).oncore[this.options.wbenv][this.id];
    this.user = this.credentials.USERNAME;
    this.robot = null;
    this.running = false;
    this.logged_in = false;
    this.shutdown_requested = false;
    this.start_time = new Date();
    this.wait = this.options.wait;
    this.count = 0;
    this.environment = this.options.wbenv;
    this.urls = this.config.environments[this.environment];
    this.logger = new wbLogger(this);
    this.status = 'starting';
    this.inputHandlers = require(global.wbDir + "/etc/default_handlers.js");
    this.source = new Request(this);
    if(this.options.debug === false) {
      this.suicide_switch = setTimeout(async function() {
        try {
          debug("Initializing Suicide Switch Triggered. Killing Waterbird process");
          let error = new wbError("Initialing Suicide Switch Triggered. Waterbird process killed.", 'waterbird_suicide');
          await error.log();
        } catch(err) {
          console.log(err);
        }
        process.exit(1);
      }, this.options.kill_wait * 60 * 1000 * 2); // Double standard wait time for login, etc.
    }
    process.on('uncaughtException', async (err) => {
      let error = new wbError(err, process.waterbird, 'uncaught_exception');
      await error.log();
      debug("UNCAUGHT EXCEPTION");
      debug(err);
      process.exit(1);
    });
    process.on('unhandledRejection', async (err) => {
      let error = new wbError(err, process.waterbird, 'unhandled_rejection');
      await error.log();
      debug("UNHANDLED REJECTION");
      debug(err);
      process.exit(1);
    });
    process.on('warning', async (err) => {
      let error = new wbError(err, process.waterbird, 'warning');
      await error.log();
      debug("WARNING");
      debug(err);
    });
    process.on('message', function(msg, reply) {
      debug("MESSAGE");
      debug(msg);
      if(msg.has('command')) {
        if(['start', 'stop', 'shutdown', 'kill', 'status', 'screenshot'].indexOf(msg.command) > -1) {
          debug("Valid command: " + msg.command);
          process.waterbird[msg.command]();
        }
      } else {
        process.waterbird.shutdown();
      }
    });
    process.on('SIGINT', async function() {
      process.waterbird.browser.removeAllListeners('disconnected');
      debug("Interrupt Signal Received");
      process.waterbird.log('info', "Interrupt Signal Received");
      process.waterbird.shutdown();
    });
    process.on('SIGTERM', async function() {
      debug("Terminate Signal Received");
      process.waterbird.log('info', "Terminate Signal Received");
      process.waterbird.kill();
      setTimeout(function() {
        process.kill(process.pid, 'SIGKILL');
      }, 40000);
    });

  }

  resetSuicideSwitch(time = null) {
    if(this.options.debug === false) {
      if(time === null) {
        time = this.options.kill_wait * 60 * 1000;
      }
      if(this.suicide_switch !== null) {
        clearTimeout(this.suicide_switch);
      }
      if(time !== 0) {
        this.suicide_switch = setTimeout(async function() {
          debug("Suicide Switch Triggered. Killing Waterbird process");
          let error = new wbError("Suicide Switch Triggered. Waterbird process killed.", 'waterbird_suicide');
          await error.log();
          process.exit(1);
        }, time);
      }
    }
  }

  sleep(msg = null, ms = null) {
    if(msg !== false) {
      if(ms === null) {
        ms = this.wait * 1;
        this.wait = Math.min(this.wait * 2, this.options.max_wait*60*1000);
      }
      let wait_msg = "Waiting " + (ms / 1000) + " seconds...";
      if(msg === null) {
        msg = wait_msg;
      } else {
        msg = msg + " " + wait_msg;
      }
      // Prevent the suicide switch from firing because of a long sleep;
      this.resetSuicideSwitch(this.wait + (this.options.kill_wait * 60 * 1000));
      this.log('info', msg);
      debug(msg);
    }
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  async log(level, msg, page = null) {
    await process.waterbird.logger.app(level, msg, page);
  }

  registerInputHandler(name, input_fn, config) {
    debug("Registering InputHandler: " + name);
    let inputHandler = new InputHandler(name, input_fn, config);
    this.inputHandlers[name] = inputHandler;
  }

  removeInputHandler(name) {
    delete this.inputHandlers[name];
  }

  setStatus(status) {
    this.status = status;
    process.send({'status': status});
  }

  reportResult(result) {
    process.send({'result': result, wbid: this.id});
  }

  async start() {
    if(!this.running) {
      this.log('info', "Data Entry Loop started");
      this.running = true;
      this.resetSuicideSwitch();
      this.setStatus('running');
      await this.updateRecord();
    }
  }

  stop(status = 'stopping') {
    if(this.running) {
      this.log('info', "Data Entry Loop stopped");
      this.resetSuicideSwitch(0);
      this.setStatus(status);
      this.running = false;
    }
  }

  async kill(clean = true) {
    this.log('info', "Killing Process");
    debug("Killing Process");
    this.running = false;
    try {
      if(process.waterbird.browser !== null) {
        process.waterbird.browser.removeAllListeners('disconnected');
        await process.waterbird.browser.close();
      }
    } catch(err) {
      debug(err);
    }
    try {
      this.setStatus('killed');
      if(clean === true) {
        debug("Exiting Cleanly");
        process.exit(0);
      } else {
        debug("Exiting Dirty");
        process.exit(1);
      }
    } catch(e) {}
  }

  async shutdown() {
    if(!this.running) {
      await this.completeShutdown();
    } else {
      this.log('info', 'Shutdown Requested');
      debug("Shutdown Requested");
      this.setStatus('shutting down');
      this.shutdown_requested = true;
    }
  }

  async completeShutdown() {
    this.log('info', 'Shutting Down Waterbird');
    debug("Shutting Down Waterbird");
    try{
      if(process.waterbird.browser !== null) {
        process.waterbird.browser.removeAllListeners('disconnected');
        await process.waterbird.browser.close();
      }
    } catch(err) {
      let error = new wbError(err, process.waterbird, 'shutdown_error');
      await error.log();
    } finally {
      this.setStatus('shutdown');
      process.exit(0);
    }
  }

  async status() {
    process.send({status: this.status});
  }

  screenshot() {
    this.page.screenshot({fullpage: true, encoding: 'base64'})
      .then((x) => {
        process.send({screenshot: x});
      }, (x) => {
        this.log('warning', x);
      });
  }

  async createBrowser() {
    if(this.browser === null || this.browser === undefined) {
      debug("Creating Browser");
      this.log('info', "Creating Browser");
      let browser_options = {
	ignoreHTTPSErrors: true,
	headless: this.options.headless,
	slowMo: this.options.slomo
      };
      if(!this.options.headless) {
	Object.assign(browser_options, {
	  args: [
            `--window-size=${ this.options.width },${ this.options.height }`,
            `--app-shell-host-window-size=${ this.options.width }x${ (this.options.height + 150) }`,
            `--content-shell-host-window-size=${ this.options.width }x${ this.options.height }`
          ]

	});
      }
      debug(browser_options);
      try {
        this.browser = await puppeteer.launch(browser_options);
        this.browser.waterbird = this;
        this.browser.on('targetcreated', async function(t) {
          debug("Page Creation Detected");
          let browser = t.browser();
          let pages = await browser.pages();
          if(pages.length > 2) {
            debug("Extra Page Closed");
            for(let i = 1; i < pages.length-1; i++) {
              await pages[i].removeAllListeners('close'); // turn off event handling for page crash
              await pages[i].close();
            }
          }
        });
        this.browser.on('disconnected', async function() {
          this.waterbird.log("warn", "Browser Disconnected");
          this.waterbird.browser = null;
          debug("Lost connection with browser");
        });
        this.wait = this.options.wait;
        this.emit("browserCreate", true);
      } catch(err) {
        let error = new wbError(err, this, 'browser_creation');
        await error.log();
        this.emit("browserCreate", false, error);
        await this.sleep("Browser Creation Failed");
      }
    }
  }

  async needToLogin() {
    debug("Checking Login Status...");
    if(this.browser === null) {
      await this.createBrowser();
    }
    let pages = await this.browser.pages();
    let page = pages[0];
    try {
      await page.reload();
      let login_test = await page.$('a#user-name');
      if(login_test) {
        debug("Logged in");
        this.logged_in = true;
        return false;
      } else {
        debug("Not logged in");
        this.logged_in = false;
        return true;
      }
    } catch(err) {
      let error = new wbError(err, process.waterbird, 'login_check');
      await error.log();
      debug(err);
      this.logged_in = false;
      return true;
    }
  }

  async login() {
    debug("Attempting Login");
    if(this.brower === null) {
      await this.createBrowser();
    }
    let pages = await this.browser.pages();
    let page = pages[0];
    await page.goto(this.urls.oncore);
    let login_test = await page.$('a#user-name');
    if(login_test === null) {
      try {
        let robot = {
          page: page,
          module: this,
          data: {
            data: this.credentials
          }
        };
        let user = new this.inputHandlers['text']({
          type: 'text',
          id: 'username',
          source: 'USERNAME',
          required: true
        }, robot);
        let pass = new this.inputHandlers['text']({
          type: 'text',
          id: 'password',
          source: 'PASSWORD',
          required: true
        }, robot);
        let submit = new this.inputHandlers['button']({
          type: 'button',
          id: "submitBtn",
          wait: "navigation"
        }, robot);
        await user.execute();
        await pass.execute();
        await submit.execute();
        await page.waitForSelector("#user-name");
        this.wait = this.options.wait;
        this.emit("login", true);
        this.setStatus('ready');
        this.log('info', 'login successful');
      } catch(err) {
        let error = new wbError(err, {robot: {page: page}}, 'login'); // Cheating to get screenshot of login page.
        await error.log();
        this.emit('login', false, error);
        await this.sleep("Login Failure");
      }
    } else {
      let error = new wbError("Already Logged In", {robot: {page: page}}, 'login'); // Cheating to get screenshot of login page.
      await error.log();
      this.emit('login', false);
    }
  }

  async updateRecord() {
    if(this.shutdown_requested === true) {
      this.running = false;
      setTimeout(function() {
        process.waterbird.completeShutdown();
      }, 2000);
    } else if(this.running) {
      while(this.browser === null || this.browser === undefined) {
        debug("Creating Browser from loop");
        await this.createBrowser();
      }
      while(await this.needToLogin()) {
        await this.login();
      }
      if(this.logged_in === true) {
        let robot = new Robot(this);
        robot.on('finished', async function(result, msg) {
          debug(msg);
          this.module.resetSuicideSwitch();
          if(result === 'success') {
            debug("Data Entry Successful");
            await process.waterbird.logger.data(this, 'success', msg);
            this.module.wait = this.module.options.wait;
          } else if(result === 'failure') {
            await process.waterbird.logger.data(this, 'failure', msg);
            debug("Data Entry Failed");
            this.module.wait = this.module.options.wait;
          } else if(result === 'data_error') {
            if(msg.type === 'No Data') {
              result = 'no_data';
	      this.module.setStatus('waiting');
              this.module.reportResult(result);
              await this.module.sleep("No Data", 600000);
            } else {
              result = 'bad_data';
              await process.waterbird.logger.data(this, 'data_error', msg);
              await this.module.sleep(msg);
            }
          }
          this.module.reportResult(result);
          let current_time = new Date();
          if(current_time - this.module.start_time > (this.module.options.max_uptime * 60 * 1000)) {
            debug("max_run_time exceeded, killing process");
            this.module.log('info', "max_run_time exceeded, process killed");
            this.module.kill(false);
          } else {
            if(this.module.options.debug === false) {
              if(this.status !== 'waiting') {
                await this.module.updateRecord();
              }
            } else {
              this.module.running = false;
            }
          }
        });
        await robot.execute();
        this.count++;
      }
    } else {
      this.setStatus('stopped');
    }
  }

}

module.exports = wbModule;
