const shortid = require('shortid');
const winston = require('winston');
require('winston-daily-rotate-file');
const {createLogger, format, transports} = require('winston');
const {combine, timestamp, printf} = format;

var debug;

class wbError extends Error{
  constructor(message, obj = null, context = null) {

    debug = require('debug')('waterbird' + process.waterbird.id + ':wbError');

    debug(message);
    if(message instanceof Error) {
      debug("Converting Error");
      super(message.message);
      this.err = message;
      this.stack = message.stack;
      this.name = message.name;
      this.code = message.code;
    } else {
      debug("Building New Error");
      super(message);
      // Maintains proper stack trace for where our error was thrown (only available on V8)
      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, wbError);
      }
      this.name = this.constructor.name;
      this.err = this;
    }
    if(obj === null) {
      this.robot = null;
    } else if(obj.constructor.name === "wbRobot"){
      this.robot = obj;
    } else if(obj.has('robot')) {
      this.robot = obj.robot;
    } else {
      this.robot = null;
    };

    this.context = context;

    this.waterbird = process.waterbird;
    this.log_dir = this.waterbird.logger.log_dir;

    this.log_format = winston.format.combine(
      winston.format.timestamp(),
      winston.format.printf(info => {
        let log = {'wbid': info.wbid,
                   'timestamp': info.timestamp,
                   'message': info.message,
                   'env': info.wbenv,
                   'user': info.user,
                   'trace': info.trace || "no trace",
                   'context': info.context || "no context",
                   };
        if(info.has('sid')) {
          log.sid = info.sid;
        };
        return JSON.stringify(log);
      }
    ));
    this.log_transport = new (winston.transports.DailyRotateFile)({
      level: 'error',
      filename: this.log_dir + 'errors-' + this.waterbird.environment + '-%DATE%.log',
      zippedArchive: false,
      maxSize: '50m',
      maxFiles: '30d'
    });
    this.logger = createLogger({
      format: this.log_format,
      transports: [
        this.log_transport,
      ],
      exceptionHandlers: [
        this.waterbird.logger.transports.exceptions
      ],
      exitOnError: false
    });

  }

  async log() {
    debug("Logging Error");
    let error_log = {
      wbid: this.waterbird.id,
      user: this.waterbird.user,
      env: this.waterbird.environment,
      message: this.message,
      trace: this.err.stack,
    };
    if(this.context !== null) {
      error_log.context = this.context;
    }
    if(this.robot != null) {
      debug("Attempting screenshot");
      try {
        error_log.sid = shortid.generate();
        debug("SID: " + error_log.sid);
        await this.robot.page.screenshot({path: this.waterbird.logger.screenshot_folder + error_log.sid + ".png", fullPage: true, type: 'jpeg', quality: 25});
        debug("Screenshot Taken");
      } catch(err) {
        debug("Screenshot Failed");
        debug(err);
        error_log.sid = null;
      }
    }
    debug("Adding to error log");
    this.logger['error'](error_log);
    debug("Adding to waterbird log");
    this.waterbird.log('error', "ERROR: " + this.message, error_log.sid || null);
  }

}

module.exports = wbError;
