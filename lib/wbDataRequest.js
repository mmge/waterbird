var debug;
const request = require('request-promise-native');
const fs = require('fs');
const yaml = require('js-yaml');
const wbError = require(global.wbDir + '/lib/wbError.js');

class wbDataRequest {

  constructor(wb) {
    debug = require('debug')("waterbird" + wb.id + ":data_request");
    debug("Creating data request object");
    let options = wb.options;
    this.options = wb.options;
    this.jwt = null;
    this.wb = wb;
    this.token_tries = 0;
    this.process = options.process;
    this.url = wb.urls.api;
    this.request_options = yaml.safeLoad(fs.readFileSync(global.wbDir + '/etc/config.yaml')).request
    if(options.wbenv == 'TST') {
      this.authenticate = false;
    } else if(['PRD', 'STG', "DEV"].indexOf(options.wbenv) != -1){
      this.authenticate = true;
      this.auth = "Basic " + new Buffer(wb.user + ":" + yaml.safeLoad(fs.readFileSync(global.wbDir + '/etc/creds.yaml')).api.password).toString('base64');
    } else {
      throw new Error("Unrecognized Environment: " + options.wbenv);
    }
    this.qs = {};
    this.body = {};
  }

  async getJWT() {
    debug("Requesting JWT from API...");
    let options = Object.assign({}, this.request_options, {
      method: 'GET',
      url: this.url + 'session',
      rejectUnauthorized: false,
      headers: {
        'Authorization': this.auth,
        'Cache-Control': 'no-cache',
        'Accept': 'application/json',
        'User-Agent': 'Waterbird',
      }
    });
    try{
      let jwt = await request(options);
      jwt = JSON.parse(jwt);
      this.jwt = jwt.token;
      this.token_tries = 0;
      this.data_tries = 0;
      debug("Token Aquired");
    } catch(err) {
      let error = new wbError(err, this.wb, 'jwt_creation');
      await error.log();
      debug(err);
      this.jwt = null;
    }
  }

  async sendRequest(endpoint, method = "GET", payload) {

    let that = this;

    if(payload !== undefined) {
      if(method === "GET") {
        Object.assign(this.qs, payload);
      } else {
        Object.assign(this.body, payload);
      }
    }

    async function api_request() {
      debug("Sending request to " + that.url);

      while(that.jwt == null && that.authenticate && that.token_tries < 5) {
        await that.getJWT();
        that.token_tries++;
      }

      let req = Object.assign({}, that.request_options, {
        method: method,
        url: that.url + endpoint,
        rejectUnauthorized: false,
        headers: {
          "Accept": "application/json",
          "User-Agent": "Waterbird"
        }
      });

      if(that.authenticate) {
        debug("Authentication Required: " + that.jwt);
        req.headers.Authorization = "Bearer " + that.jwt;
      }

      if(method == "GET" & Object.keys(that.qs).length > 0) {
        req.qs = that.qs;
      } else if(Object.keys(that.body).length > 0) {
        req.body = that.body;
        req.headers['Content-Type'] = 'application/json';
      }

      try {
        let response = await request(req);
        response = await JSON.parse(response);
        if(response.has("result")) {
          if(response.result === 'error') {
            if(response.message === "Authentication failed. Aborted.") {
              that.jwt = null;
              return false;
            } else {
              let error = new wbError(response.message, that.wb, 'api_data_request');
              await error.log();
              debug(err);
              return false;
            }
          } else {
            return response.message;
          }
        } else {
          return response;
        }
      } catch(err) {
        let error = new wbError(err, that.wb, 'api_data_parsing');
        await error.log();
        debug(err);
        debug(response);
        return false;
      }

    }

    try {

      let response = await api_request();
      while(!response && this.api_tries < 5) {
        response = api_request();
        this.api_tries++;
      }
      return response;

    } catch(err) {

      let error = new wbError(err, that.wb, 'api_data_return');
      await error.log();
      debug(err);
      debug(response);
      return false;

    }

  }

  async requestVerify() {
    let response = await this.sendRequest('runVerify', "GET");
    return response.message === "OK";
  }

  async requestStats() {
    return await this.sendRequest('getStats', "GET");
  }

  async getData() {

    if(this.options.has('entity_id')) {
      this.qs.BARCODE = this.options.entity_id;
    } else {
      if(this.options.has('type')) {
        this.qs.TYPE = this.options.type;
      }
      if(this.options.has('mode')) {
        this.qs.MODE = this.options.mode;
      }
    }

    let data = await this.sendRequest('getEntity', "GET");

    let result;

    if(!data) {
      result = {
        type: "error",
        mode: "error",
        data: data
      };
    } else if(data.has('error')){
      debug(data.error);
      result = {
        type: 'error',
        mode: 'error',
        data: data
      };
    } else {
      result = data;
    }

    return result;

  }

}

module.exports = wbDataRequest;
