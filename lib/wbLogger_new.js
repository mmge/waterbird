const fs = require('fs');
const yaml = require('js-yaml');
const shortid = require('shortid');

var debug;

class wbLogger{

  constructor(wb) {

    debug = require('debug')(`waterbird ${process.waterbird.id}:logger`);
    let that = this;
    this.waterbird = wb;
    let d = new Date();
    this.config = yaml.safeLoad(fs.readFileSync(global.wbDir + '/etc/config.yaml')).logs;
    let db_creds = yaml.safeLoad(fs.readFileSync(global.wbDir + '/etc/creds.yaml')).log_database;
    this.db_config = {
      user: db_creds.user,
      password: db_creds.password,
      server: this.config.database.server,
      database: this.config.database.database,
      pool: this.config.database.pool
    };
    this.entity_fields = this.config.entity_fields;
    this.pool = "";

  }

  async connect() {

    debug(typeof this.pool);
    if(this.pool === "") {
      debug("Building connection...");
      try{
        this.pool = await mssql.connect(this.con_config);
        debug("Connected to mssql for logging");
      } catch(err) {
        debug(err);
      }
    }

  };

  async close() {
    await this.pool.close();
  }

  async request(query) {
    let p = await this.get_pool();
    try {
      const result = await p.query(query);
      return result;
    } catch(err) {
      debug(err);
    }
  }

  make_entity_query(data, fields) {

    let f = [];
    let v = [];

    for(let i = 0; i < fields.length; i++) {
      if(Object.keys(data).indexOf(fields[i]) > -1) {
        f.push(fields[i]);
        v.push("'" + data[fields[i]] + "'");
      }
    }

    f.push('timestamp');
    v.push('getdate()');

    let f_str = f.join(",");
    let v_str = v.join(",");
    let q = `INSERT INTO dbo.entities (${f_str}) VALUES (${v_str})`;

    debug(q);

    return q;

  }

  make_values_query(data, fields) {

    let n = [];
    let entity_id = data.entity_id;
    let keys = Object.keys(data);

    for(let i = 0; i < keys.length; i++) {
      if(fields.indexOf(keys[i]) == -1) {
        n.push(`('${entity_id}','${keys[i]}','${data[keys[i]]}')`);
      }
    }
    let values = n.join(",");

    let q = `INSERT INTO dbo.log_values (entity_id, name, value) VALUES ${values};`;

    debug(q);

    return q;

  }

  async log(log_data, callback) {

    log_data.entity_id = shortid.generate();

    try {
      let entity_query = this.make_entity_query(log_data);
      debug(await this.request(entity_query));
    } catch(err) {

    }

    try {
      let values_query = this.make_values_query(log_data);
      debug(await this.request(values_query));
    } catch(err) {

    }

  }

}
