const argv = require('yargs').argv;
const debug = require("debug")("waterbird" + argv.id + ":child_process");
const debug_rcv = require("debug")("waterbird" + argv.id + ":child_message_received");
const fs = require('fs');
const yaml = require('js-yaml');
const path = require('path');

global.wbDir = path.resolve(__dirname + "/..");
const Waterbird = require(global.wbDir + '/lib/wbModule.js');

var waterbird = new Waterbird(argv);

waterbird.registerInputHandler("dojo_select",
async function(step) {
  this.debug("dojo_select: " + step.xpath + ": " + step.value);
  this.debug("Finding element");

  async function type(el, value) {

    let i = 0;

    console.log(value);

    while(i < value.length) {

      let c = value.substring(i,++i);
      if(c === "&") {
	console.log("Typing Ampersand");
//	step.page.keyboard.press("AltLeft");
	step.page.keyboard.down('AltLeft');
        await el.type('0038');
        step.robot.page.keyboard.up("AltLeft");
      } else {
        await el.type(c);
      }

    }

  }

  let value = step.value;//.replace("&", "\\&");
  console.log(step.value);
  let initial_entry_length = Math.floor(value.length / 2);
  let eh = await this.findElement(step, step.xpath + "/parent::span");
  this.debug("Finding subelement");
  let x = await eh.$("input.dojoComboBox");
  let xp = "//div[contains(@class, 'dojoComboBoxItem')]/span[text()='" + step.value + "']";
  await x.click({clickCount: 3});
  this.debug("Typing value: " + value);
  let p1 = value.substring(0,initial_entry_length);
  this.debug("Typing value: " + p1);
  await type(x, p1);

//  await type(x, step.value);

  try {
    this.debug("Looking for dojo_select options");
    await step.page.waitForXPath("//div[contains(@class, 'dojoComboBoxItem')]/span[contains(text(), '" + p1 + "')]");
  } catch(err) {
    await this.processError(err, step);
    return false;
  }


  if(step.value.length > initial_entry_length) {

    let s = value.substr(initial_entry_length, value.length);
    await type(x, s);

/*
    let i = initial_entry_length;
    let check = [];
    while(i < (step.value.length) && check.length === 0) {
      try {
        check = await step.page.waitForXPath(xp, {timeout: 2000});
      } catch(err) {
        await x.type(step.value.substring(i,++i), {delay: 5});
      }
    }
*/

  }

  this.debug("Waiting for popup");
  try {
    this.debug(xp);
    await step.page.waitForXPath(xp);
  } catch(err) {
    await this.processError(err, step);
    return false;
  }
  this.debug("Clicking Popup element");
  try{
    let y = await this.findElement(step, xp);
    await y.click();
    await step.page.waitFor(250);
  } catch(err) {
    await this.processError(err, step);
    return false;
  }
  this.debug("Dojo select complete");
  return true;
},
{
  check_fn: async function(step) {
    this.debug("dojo_select: " + step.xpath + ": " + step.value);
    let eh = await this.findElement(step, step.xpath + "/parent::span");
    let x = await eh.$("input.dojoComboBox");
    let val = await step.page.evaluate(el => {return el.value;}, x);
    return this.check(val, step.value, step.config.exact);
  }
});

waterbird.registerInputHandler('date_time',
  async function(step) {
    this.debug("date_time: " + step.xpath + ": " + step.value);
    try {
      let date_re = /[0-9]{2}\/[0-9]{2}\/[0-9]{4}/;
      let time_re = /[0-9]{2}:[0-9]{2}/;
      let date = date_re.exec(step.value)[0];
      let time = time_re.exec(step.value)[0];
      let eh = await this.findElement(step, step.xpath);
      let date_field = await eh.$('input.date');
      await date_field.type(date, {delay: 5});
      let time_field = await eh.$('input.time');
      await time_field.click({clickCount: 3});
      await time_field.type(time, {delay: 5});
    } catch(err) {
      await this.processError(err, step);
      return false;
    }
    return true;
  }
);

waterbird.registerInputHandler('select2',
  async function(step) {
    this.debug("select2: " + step.xpath + ": " + step.value);
    try {
      let eh = await this.findElement(step, step.xpath);
      await eh.click();
      let ch = await step.page.$x("//ul[@class='select2-results']//div[text()='" + step.value + "']");
      ch = ch[0];
      await ch.click();
    } catch(err) {
      await this.processError(err, step);
      return false;
    }
    return true;
  }
);

waterbird.registerInputHandler('specimen_location',
  async function(step) {

    this.debug("## SPECIMEN_LOCATION ENTRY ##");
    let page = step.page;
    let that = this;

    if(!step.has('location_failed')) {
      step.location_failed = false;
    }

    async function openDialog() {
      try {
        if(await page.$("select[name='storage_location_id']") === null) {
          await that.subStep({
            type: 'button',
            id: 'storageText',
            log_errors: false,
            wait: 100,
          }, step);
          await page.waitFor("select[name='storage_location_id']");
          await page.waitFor(100); // little pauses to hopefully help OnCore not freak out
        }
      } catch(err) {
        that.debug(err);
        return false;
      }
      that.debug("Location Dialog Opened");
      return true;
    }

    async function enterLocations(locations, current_step = 0, tries = 0) {
      that.debug("Attempting Location " + current_step + " Entry: " + tries);
      let pause = 50 + (tries * 250);
      if(tries < 3) {
        try{
          that.debug("Interacting with: " + "select[name='" + locations[current_step].name + "']");
          // check to see if the select box has been populated. (i.e., There are at least 2 options present, there is always a blank option)
          await page.waitFor("select[name='" + locations[current_step].name + "'] > option:nth-child(2)", {timeout: 3000});
          that.debug("Found Element");
          await step.page.waitFor(pause); // little pauses to hopefully help OnCore not freak out
          let result = await that.subStep(locations[current_step], step, true);
          await step.page.waitFor(pause); // little pauses to hopefully help OnCore not freak out
          that.debug(result.result);
          switch(result.result) {
            case 'success':
              current_step++;
              if(current_step >= locations.length) {
                return true;
              } else {
                return enterLocations(locations, current_step, tries);
              }
              break;
            default:
              that.debug("Increasing pause to " + (tries*500) + "ms.");
              tries++;
              current_step = 0;
              return enterLocations(locations, current_step, tries);
              break;
          }
        } catch(err) {
          that.debug(err);
          that.debug("Increasing pause to " + (tries*500) + "ms.");
          tries++;
          current_step = 0;
          return enterLocations(locations, current_step, tries);
        }
      } else {
        that.debug("Location entry failed too many times... Aborting");
        return false;
      }
    }

    async function closeDialog(success) {
      try{
        if(success) {
          await that.subStep({
            type: "button",
            id: "saveStorageResults",
            timeout: 5000,
            log_errors: false,
          }, step);
        } else {
          await that.subStep({
            type: "button",
            id: "closeStorageResults",
            timeout: 5000,
            log_errors: false,
          }, step);
        }
        await page.waitForSelector("select[name='storage_location_id']", {hidden: true, timeout: 2000});
        await page.waitForSelector(".dialogUnderlay", {hidden: true, timeout: 2000});
        await page.waitFor(100); // little pauses to hopefully help OnCore not freak out
      } catch(err) {
        that.debug(err);
        that.debug("Location Dialog Failed to Close");
        return false;
      }
      that.debug("Locations Dialog Closed");
      return true;
    }

    async function execute() {
      if(await openDialog()) {
        let success = await enterLocations(locations);
        if(!success) {
          success = await enterLocations(default_locations);
        }
        let close_attempts = 0;
        while(await closeDialog(success) === false) {
          close_attempts++;
          await page.waitFor(500);
          if(close_attempts > 3) {
            return false;
          }
        }
        return success;
      } else {
        return false;
      };
    }

    let locations = [
      {
        type: 'select',
        name: 'storage_location_id',
        source: 'STORAGE_LOCATION_ID',
        log_errors: false,
      },
      {
        type: 'select',
        name: 'storage_unit_id',
        source: 'STORAGE_UNIT_ID',
        log_errors: false,
        match_type: 'partial',
      }
    ];

    let default_locations = [
      {
        type: 'select',
        name: 'storage_location_id',
        value: 'TK',
        log_errors: false,
      },
      {
        type: 'select',
        name: 'storage_unit_id',
        value: 'IUGB_Waterbird',
        log_errors: false,
        match_type: 'partial',
      }
    ];

    // Builds the variable index locations and adds them to the locations array
    let name_index = 1;
    for(let source_index = 1; source_index <= 5; source_index++) {
      let name = "index_no_" + name_index;
      let source = "INDEX_NO_" + source_index;
      if(step.data.has(source)) {
        let index_step = {
          type: 'select',
          name: name,
          match_type: 'regex',
          value: "^" + step.data[source] + "\\s",
          timeout: 1000,
          log_errors: false,
        };
        locations.push(index_step);
        name_index++;
      }
    }

    let attempts = 0;
    while(await execute() === false) {
      attempts++;
      if(attempts > 3) {
        await closeDialog();
        return false;
        break;
      }
    }
    return true;

  }
);

waterbird.registerInputHandler('clear_spec_coll',
  async function(step) {
    this.debug("clear_spec_coll: " + step.xpath + ": " + step.value);
    try {
      try {
        await step.page.waitForXPath("//input[@name='confirm_config_id']", {
          timeout: 1000
        });
      } catch(err) {
        return true; // Skip the rest if there are no boxes to uncheck.
      }
      await step.page.waitFor(500);
      let cci = await step.page.$x("//input[@name='confirm_config_id']");
      for(let i = 0; i < cci.length; i++) {
        step.page.evaluate(cb => {
          cb.checked = false;
        }, cci[i]);
      }
    } catch(err) {
      await this.processError(err, step);
      return false;
    }
    return true;
  },
  {
    xpathRequired: false,
    valueRequired: false,
  }
);

waterbird.registerInputHandler('set_status', async function(step) {

  let that = this;

    async function clickButton(name, extra_step_properties) {
      that.debug("Set_Status Name: " + name);
      let x = await step.page.$("input[name='" + name + "']");
      if(x !== null) {
        that.debug("Updating Specimen Status");
        let sub_step = Object.assign({}, {type: 'button', name: name}, extra_step_properties);
        await that.subStep(sub_step, step);
      } else {
        that.debug("Specimen Status Already Set");
        await that.subStep({type: 'button', name: 'submitBtnTop'}, step);
      };
    }

    await step.page.evaluate(() => {
      document.documentElement.scrollTop = 0;
    });

    if(step.value !== null) {

      this.debug("set_status: " + step.value);
      try {
        let x;
        switch(step.value) {
          case 'New':
	    this.debug("SPECIMEN_STATUS: New");
            await clickButton('undoCheckinBtn');
            break;
          case 'Available':
	    this.debug("SPECIMEN_STATUS: Available");
            await clickButton('checkInBtn');
            break;
          case 'Deaccessioned':
	    this.debug("SPECIMEN_STATUS: Deaccessioned");
            await clickButton('deaccessionBtn', {dialogs: [{type: 'confirm', message: 'Are you sure you want to Deaccession this specimen?', action: 'accept'}]});
            break;
          case 'Destroyed':
	    this.debug("SPECIMEN_STATUS: Destroyed");
            await that.subStep({
              type: "button",
              name: "destroyBtn",
              timeout: 5000,
              log_errors: false,
            }, step);
            await that.subStep({
              type: "select",
              name: "dlg_reason_destroyed",
              timeout: 5000,
              log_errors: false,
            }, step);
            await that.subStep({
              type: "button",
              xpath: "//a[contains(@href='#undefinedSubmit')]",
              timeout: 5000,
              log_errors: false,
            }, step);
            break;
          case 'Reserved':
	    this.debug("SPECIMEN_STATUS: Reserved");
            await clickButton('reserveBtn');
            break;
        }
      } catch(err) {
        await this.processError(err, step);
        return false;
      }
      return true;
    } else {
      try {
        await that.subStep({type: 'button', name: 'submitBtnTop'}, step);
      } catch(err) {
        await this.processError(err, step);
        return false;
      }
      return true;
    }

}, {
  xpathRequired: false,
  valueRequired: false,
});

waterbird.registerInputHandler('annotations',
  async function(step) {
    this.debug("annotations: " + step.xpath + ": " + step.value);
    let script_loc = global.wbDir + "/etc/" + step.value + ".yaml";
    try {
      var anno_script = yaml.safeLoad(fs.readFileSync(script_loc));
    } catch(err) {
      this.debug(err);
      this.debug("No Script for " + step.value);
      await this.processError(err, step);
      return false;
    }
    for(let i = 0; i < anno_script.length; i++) {
      let xpath = '//li[./span/span/span[@class="label-label" and text()="' + anno_script[i].label + '"]]';
      if(step.data.has(anno_script[i].label)) {
        this.debug("Required Data Found: " + anno_script[i].label);
        let eh = await step.page.$x(xpath, {timeout: 50});
        if(eh.length > 0) {
          this.debug("Found Input element: " + anno_script[i].label);
          eh = eh[0];
          let label = await eh.$eval(".form-label", (el) => {
            return el.getAttribute("field-label");
          });
          this.debug("field-label: " + label);
          let anno_step = {
            type: anno_script[i].type,
            xpath: "//*[@field-id='" + label + "']",
            data: step.data,
            value: step.data[anno_script[i].label],
            log: this.log,
          };
          if(await this.subStep(anno_step, step)) {
            this.debug("Annotation Step Successful");
          } else {
            this.debug("Annotation Step Failed");
            this.debug(anno_step);
            return false;
            break;
          }
        }
      }
    }
    return true;
  },
  {
    xpathRequired: false,
  },
);

waterbird.registerInputHandler('parent_select',
  async function(step) {
    let case_step = {
      "type": "dojo_select",
      "name": "case_no_selected",
      "source": "CASE_NO",
      "wait": "//span[contains(@class, 'dojoTreeNodeLabelSelected') and contains(text(), '<CASE_NO>')]",
      "condition": {
        "xpath": "//span[contains(@class, 'dojoTreeNodeLabelSelected') and contains(text(), '<CASE_NO>')]",
        "exists": false
      },
      verify: false
    };
    let specimen_step =  {
      "type": "dojo_select",
      "name": "specimen_no_selected",
      "source": "PARENT_SPECIMEN_BARCODE",
      "wait": "//td[contains(@class, 'field-data') and contains(text(), '<PARENT_SPECIMEN_BARCODE>')]",
      "condition": {
        "xpath": "//td[contains(@class, 'field-data') and contains(text(), '<PARENT_SPECIMEN_BARCODE>')]",
        "exists": false
      },
      verify: false
    };
    let result = false;
    if(step.data.has("PARENT_SPECIMEN_BARCODE")) {
      result = await this.subStep(specimen_step, step);
    } else if(step.data.has("CASE_NO")) {
      result = await this.subStep(case_step, step);
    }
    return result;
  },
  {
    xpathRequired: false,
    valueRequired: false,
  },
);


waterbird.registerInputHandler("set_count",
  async function(step) {
    let result = false;
    if(step.data.has("PARENT_SPECIMEN_BARCODE")) {
      result = await this.subStep({
        type: 'text',
        name: 'numAliquots',
        value: '1'
      }, step);
    } else {
      result = await this.subStep({
        type: 'text',
        name: 'numSpecimens',
        value: '1'
      }, step);
      if(result) {
        result = await this.subStep({
          type: 'text',
          name: 'numAliquots',
          value: '0'
        }, step);
      }
    }
    return result;
  },
  {
    valueRequired: false,
    xpathRequired: false,
  },
);
