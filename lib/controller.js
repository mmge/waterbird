const fork = require('child_process').fork;
const debug = require('debug')('waterbird:controller');
const debug_msg = require('debug')('waterbird:parent_message_sent');
const debug_rcv = require('debug')('waterbird:parent_message_received');
const path = require('path');
const fs = require('fs');
const yaml = require('js-yaml');
const nodemailer = require("nodemailer");

global.wbDir = path.resolve(__dirname + "/..");
const API = require(global.wbDir + "/lib/wbAPI.js");
const wbError = require(global.wbDir + '/lib/wbError.js');
const wbLogger = require(global.wbDir + '/lib/wbLogger.js');
const Request = require(global.wbDir + "/lib/wbDataRequest.js");

async function sendEmail(subj, msg, html = false, to) {

  let config = yaml.safeLoad(fs.readFileSync(global.wbDir + '/etc/config.yaml')).email;
  let creds = yaml.safeLoad(fs.readFileSync(global.wbDir + '/etc/creds.yaml')).email;

  if(to === undefined) {
    to = config.email_to;
  }

  if(Array.isArray(to)) {
    to = to.toString();
  }

  let transporter = nodemailer.createTransport({
    host: config.host,
    port: config.port,
    secure: config.secure,
    auth: creds,
  });

  let x = {
    from: "hgreport@iu.edu",
    to: to,
    subject: subj,
  };

  if(html === true) {
    x.html = msg;
  } else {
    x.text = msg;
  }

  try{
    let info = await transporter.sendMail(x);
  } catch(err) {
    console.error(err);
  }

}

Object.prototype.has = function(key) {
  return Object.keys(this).indexOf(key) > -1;
};

class wbController {
  constructor(options) {
    debug("Creating Waterbird Controller");
    process.waterbird = this;
    this.options = options;
    this.environment = options.wbenv;
    this.id = 'controller';
    this.user = 'controller';
    this.port = yaml.safeLoad(fs.readFileSync(global.wbDir + '/etc/config.yaml')).environments[this.environment].port;
    this.statusCheck_options = yaml.safeLoad(fs.readFileSync(global.wbDir + '/etc/config.yaml')).status_check;
    this.api = new API(this).start();
    this.shutting_down = false;
    this.max_children = yaml.safeLoad(fs.readFileSync(global.wbDir + '/etc/creds.yaml')).oncore[this.environment].length;
    this.children = [];
    this.start_time = new Date();
    this.fail_safe = null;

    if(typeof(options.bot_ids) === 'undefined') {
      debug("Creating bot_count children");
      this.bot_count = typeof(options.bots) === 'undefined' ? this.max_children : Math.min(options.bots, this.max_children);
      this.bot_ids = Array.apply(0, Array(this.bot_count)).map(function(cv, i) {return i;});
    } else {
      debug("Creating bot_ids children");
      if(Array.isArray(options.bot_ids) === false) {options.bot_ids = [options.bot_ids];};
      this.bot_ids = options.bot_ids.filter(function(id) {return id <= this.max_children;}, process.waterbird);
      this.bot_count = this.bot_ids.length;
    }

    this.logger = new wbLogger(this);
    this.bot_status = [];
//    this.data_source = new Request(this);
    debug(this.bot_ids);
    this.bot_ids.forEach(function(id) {
      try {
        debug(id);
        process.waterbird.createChild(id, true);
      } catch(err) {
        debug(err);
        process.exit(1);
      }
    });

    this.status_stats = {
      count: 0,
      success: 0,
      failure: 0,
      no_data: 0,
      bad_data: 0,
    };

    this.sendStatusEmail = async function(s, o) {
      let fr = s.failure / s.count;
      let subj = `Waterbird exceeding failure tolerances (${Math.round(s.failure / s.count * 100, 0)}%)`;
      let msg = "Waterbird has detected a failure rate that exceeds set tolerances.  ";
      if(fr >= stop_cut) {
        msg = msg + "This error rate indicates that waterbird should shutdown until debuging can occur.  ";
      } else {
        msg = msg + "Waterbird will continue to run but debugging should occur as soon as possible.  ";
      }
      await sendEmail(subj, msg);
    };

    this.statusCheck = setInterval(async function(wb) {
      let s = wb.status_stats;
      let o = wb.statusCheck_options;
      if((s.bad_data / s.count) > 0.5) {
        await sendEmail("Waterbird bad_data alert", "Waterbird reporting large number of 'bad_data' errors from API. You might want to look into that.");
      } else if((s.no_data / s.count) > 0.5) {
        await sendEmail("Waterbird no_data alert", "Waterbird not receiving data from API. You might want to look into that.");
      } else if(s.count > o.ignore_count) {
        let error_rate = s.failure / s.count;
        if(error_rate >= o.email_cut) {
          // Send and email
          debug("Sending emails about waterbird failures.");
          await wb.sendStatusEmail(s, o);
          // reset status counters
          wb.status_stats = {
            count: 0,
            success: 0,
            failure: 0,
            no_data: 0,
            bad_data: 0,
          };
        }
        if(error_rate >= o.stop_cut) {
          // Shutdown waterbird
          debug("To many failures. Shutting down waterbird");
          process.waterbird.gracefulShutdown();
        }
      }
    }, this.statusCheck_options.interval * 60000, this);

    process.on("SIGTERM", function() {
      process.waterbird.gracefulShutdown();
    });

    process.on("SIGINT", function() {
      process.waterbird.gracefulShutdown();
    });

  }

  async log(level, msg, page = null) {
    await process.waterbird.logger.app(level, msg, page);
  }

  gracefulShutdown() {
    if(this.shutting_down === false) {
      this.shutting_down = true;
      debug("Attempting graceful waterbird shutdown");
      for(let i = 0; i < process.waterbird.children.length; i++) {
        try {
          debug("Sending shutdown command to child " + i);
          let child = process.waterbird.children[i];
          child._wbShutdown();
        } catch(err) {
          debug(err);
        }
      }

      this.fail_safe = setTimeout(function() {
        debug("SIGINT timed out. Killing Waterbird");
        for(let i = 0; i < process.waterbird.children.length; i++) {
          try {
            let child = process.waterbird.children[i];
            child.kill('SIGTERM');
          } catch(err) {
            debug(err);
          }
        }
        try {
          process.waterbird.api.server.close();
        } catch(err) {
          console.log(err);
        }
        process.kill(process.pid, 'SIGKILL');
      }, 45000);
    } else {
      debug("Immediate Shutdown Requested");
      for(let i = 0; i < process.waterbird.children.length; i++) {
        try {
          let child = process.waterbird.children[i];
          child.kill('SIGTERM');
        } catch(err) {
          debug(err);
        }
      }
      clearTimeout(this.fail_safe);
      try {
        process.waterbird.api.server.close();
      } catch(err) {
        console.log(err);
      }
      process.kill(process.pid, 'SIGKILL');
    }

  }

  checkAllReady() {
    let all_ready = true;
    for(let i = 0; i < this.children.length; i++) {
      if(this.children[i]._wbFirstLogin == false) {
        all_ready = false;
        break;
      }
    }
    if(all_ready === true) {
      console.log("All bots report successful login");
      process.send('ready');
    }
  }

  async checkAllWaiting() {
    let all_waiting = true;
    for(let i = 0; i < this.children.length; i++) {
      if(this.children[i]._wbStatus !== 'waiting') {
        all_waiting = false;
        break;
      }
    }
    if(all_waiting && this.options.autoresume === false) {
      sendEmail("Waterbird Completed Data Entry", "The API reports 'no_data' and `autoresume`==false. Waterbird is exiting while verify runs.");
      process.kill(process.pid, 'SIGKILL');
    }
    return all_waiting;
  }
/*
  async requestVerify() {
    if(this.checkAllWaiting()) {
      while (!this.verifying) {
        this.verifying = await this.source.requestVerify();
      }
    }
  }
*/
  //check if all bots are dead, if they are, shut down
  shutdown() {
    let shouldShutDown = true;
    let code = 0; // Exit cleanly;
    for(let i = 0; i < this.children.length; i++) {
      if(['shutdown', 'killed', 'error'].indexOf(this.children[i]._wbStatus) < 0) {
        shouldShutDown = false;
        if(this.children[i]._wbStatus !== 'shutdown') {
          code = 1; // exit dirty
        }
        break;
      }
    }
    if(shouldShutDown === true) {
      debug("No Waterbird Bots running, shutting down");
      debug("Stopping API server...");
      process.waterbird.api.server.close();
      debug("Exiting Controller Process");
      process.exit(code);
    }
  }

  buildChild(id, args) {
    try {
      this.children[id] = fork(global.wbDir + '/lib/child_process.js', args, {detached: true});
      this.children[id]._wbId = id;
      this.children[id]._wbStatus = 'initializing';
      this.children[id]._wbFirstLogin = false;
      this.children[id]._wbController = this;
      this.children[id]._wbArgs = args;
      this.children[id]._wbSend = async function(msg) {
        return await this.send(msg);
      };
      this.children[id]._wbStart = function() {
        this.send({'command': 'start'});
      };
      this.children[id]._wbStop = function() {
        this.send({'command': 'stop'});
      };
      this.children[id]._wbKill = function() {
        this.send({'command': 'kill'});
      };
      this.children[id]._wbShutdown = function() {
        this.send({'command': 'shutdown'});
      };
      this.children[id].on('error', async function(err) {
        let status;
        let error = new wbError(err, null, "controller_child_error");
        await error.log();
        debug("************ CHILD ERROR ********************");
        debug(err);
        try {
          status = this._wbSend("status");
        } catch(err) {
          status = 'dead';
        }
        if(status !== 'dead') {
          this.kill();
        }
        this.waterbird.buildChild(this._wbId, this.wbArgs);
      });
      this.children[id].on('exit', async function(code, signal) {
        debug("wb" + this._wbId + " closed");
        let restart = false;
        if(this.wbStatus !== 'shutdown') {
          if(code !== null) {
            if(code !== 0) {
              restart = true;
            }
          }
        }
        if(restart) {
          try {
            this._wbController.children[this._wbId] = null;
            this._wbController.createChild(this._wbId);
            this._wbController.children[this._wbId]._wbStart();
          } catch (err) {
            let error = new wbError(err, null, "controller_child_exit");
            await error.log();
            debug(err);
          }
        }
      });
      this.children[id].on('message', async function(m) {
        if(m.has('stats')) {
          this._wbController.api.broadcast({'wbid': id, 'stats': m.stats});
        }
        if(m.has('status')) {
          debug("Status Update: wb" + id + " - " + m.status);
          this._wbStatus = m.status;
          this._wbController.api.broadcast({'wbid': id, 'status': m.status});
          if(m.status === 'killed' || m.status === 'shutdown' || m.status === 'error') {
            this._wbController.shutdown();
          } else if(m.status === 'waiting') {
            this._wbController.checkAllWaiting();
          } else if(m.status === 'ready') {
	    if(this._wbFirstLogin == false) {
	      this._wbFirstLogin == true;
              this._wbController.checkAllReady();
	    }
	  }
        }
        if(m.has('result')) {
          this._wbController.updateStatusStats(m.result);
        }
        if(m.has('screenshot')) {
          debug("Screenshot received");
//          this._wbController.api.
        }
      });
    } catch(err) {
      let error = new wbError(err, null, 'build_child');
      error.log();
    }
  }

  updateStatusStats(result) {
    this.status_stats.count++;
    this.status_stats[result]++;
  }

  createChild(id, start = false) {
    console.log("####### ID: " + id);
    if(id < this.max_children) {
      debug("Starting Waterbird Child: " + id);
      let args = [];
      for(let i = 0; i < Object.keys(this.options).length; i++) {
        let key = Object.keys(this.options)[i];
        if(key.length > 1) {
          args[i] = "--" + key + "=" + this.options[key];
        }
      }
      args[args.length] = "--id=" + id;
      this.buildChild(id, args);
      if(start === true) {
        process.waterbird.children[id]._wbStart();
      }
    };
  }

  count() {
    let x = 0;
    for(let i = 0; i < this.children.length; i++) {
      let child = this.children[i];
      x = x + child._wbStatus.count;
    }
    return x;
  }
  success() {
    let x = 0;
    for(let i = 0; i < this.children.length; i++) {
      let child = this.children[i];
      x = x + child._wbStatus.success;
    }
    return x;
  }
  failure() {
    let x = 0;
    for(let i = 0; i < this.children.length; i++) {
      let child = this.children[i];
      x = x + child._wbStatus.failure;
    }
    return x;
  }
  botTime() {
    debug("botTime");
    let x = 0;
    for(let i = 0; i < this.children.length; i++) {
      let child = this.children[i];
      x = x + child._wbStatus.uptime;
    }
    return x;
  }
  botCount() {
    debug("botCount");
    let x = 0;
    for(let i = 0; i < this.children.length; i++) {
      let child = this.children[i];
      if(child._wbStatus.status === 'running') {
        x++;
      }
    }
    return x;
  }
  uptime() {
    let up =  new Date() - this.start_time;
    return up;
  }
  formatTime(time) {
    let times = {
      s: Math.round(time / 1000),
      M: Math.round(time / 1000 / 60),
      H: Math.round(time / 1000 / 60 / 60),
      d: Math.round(time / 1000 / 60 / 60 / 24)
    };
    if(times.d >= 1) {
      return times.d + "d";
    } else if (times.H >= 1) {
      return times.H + "h";
    } else if (times.M >= 1) {
      return times.M + "m";
    } else {
      return times.s + "s";
    }
  }
  sendCommand(cmd, id, args) {
    let m = Object.assign({'type': cmd}, args);
    debug_msg("Sending '" + cmd + "' to waterbird_" + id);
    return this.children[id].send(m);
  }
  status() {
    let status = {
      environment: this.environment,
      child_count: this.children.length,
      api_port: this.api.port,
      start_time: this.start_time,
      total_records: this.count(),
    };
    return status;
  }
}

module.exports = wbController;
