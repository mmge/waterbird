const fs = require('fs');
const express = require("express");
const expressWS = require("express-ws");
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const path = require('path');

var debug = require('debug')('waterbird:api');
var debug_broadcast = require('debug')('waterbird:broadcast');

function getScreenshot(path, callback, timeout = 1000) {
  const t = setInterval(function() {
    debug("Waiting for Screenshot");
    const file = path;
    const fileExists = fs.existsSync(file);
    if(fileExists) {
      debug("Screenshot Found");
      callback(path);
      clearInterval(t);
    }
  }, timeout);
}

function readDataLog(type, date, env = 'TST') {
  let fn = "/var/log/waterbird/" + type + "-" + env + "-" + date + ".log";
  try {
    let raw_data = fs.readFileSync(fn).toString();
    let x = "[" + raw_data.replace(/\n{/g, ",\n{") + "]";
    let data = JSON.parse(x);
    return data;
  } catch(err) {
    return [];
  }
}

class wbAPI {

  constructor(controller, version = 0) {

    let api = express();
    api.waterbird = controller;
    this.controller = controller;
    this.port = controller.port;
    this.api = api;
    this.wss = expressWS(api).getWss('/a');
    this.server = null;
    debug(version);
    api.use(express.static('public'));
    api.use(function(req, res, next) {
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
      next();
    });
    let base_url = "/api/v" + version;

    // Register Endpoint
    /*
    api.post(base_url + "register", function(req, res) {
      let hashedPassword = bcrypt.hashSync(req.body.password, 8);
      User.create({
        name: req.body.name,
        email: req.body.email,
        password: hashedPassword
      },
      function(err, user) {
        if(err) (function(resolve, reject) {
          debug(err);
          return res.status(500).send("There was a problem registering the user.");
        }
        let token = jwt.sign({id: user._id}, config.secret);
      });
    });
*/
    // Set up the websocket;
    api.ws(base_url + "/ws", function(ws, req) {
        ws.on('message', async function(msg) {
          //debug_broadcast(msg);
          msg = JSON.parse(msg);
          if(msg.id !== 'controller') {
            debug("Passing message to child");
            api.waterbird.sendCommand(msg.msg, msg.id, {});
          } else if(msg.msg === 'createChild') {
            debug("Creating Child");
            await api.waterbird.createChild();
            await api.waterbird.sendCommand('start');
          }
      });
    });

    api.get(base_url + "logs/screenshot/:id", async function(req, res) {
      let id = req.params.id;
    });

    api.get(base_url + "/logs/:type/:date", async function(req, res) {
      let type = req.params.type;
      let date = req.params.date;
      res.header("Content-Type", "application/json");
      res.send(readDataLog(type, date));
    });

    api.get(base_url + "/stats/:date", async function(req, res) {

      let raw_data = readDataLog("data_entry", req.params.date);
      let stats = {
        total: 0,
        success: 0,
        failure: 0,
        subject: 0,
        update: 0,
        insert: 0,
        wb0: 0,
        wb1: 0
      };
      raw_data.forEach(function(de) {
        stats.total++;
        stats[de.result]++;
        stats[de.mode] ++;
        stats[de.type]++;
        stats["wb"+de.wbid]++;
      });
      res.json(stats);

    });

    api.get(base_url + "/children/:id", async function(req, res) {
      let id = req.params.id;
      res.json(controller.children[id]._wbStatus);
    });
    api.get(base_url + "/children/:id/:type", async function(req, res) {
      let id = req.params.id;
      let type = req.params.type;
      let msg = "REST: GET waterbird_" + id + " - " + type;
      controller.children[id]._wbSend(type);
      let r = {'id': id,
               'type': type,
               'value': controller.children[id]._wbStatus[type]};
      if(type !== 'screenshot') {
        res.writeHead(200, {'Content-Type': 'application/json'});
        res.end(JSON.stringify(r));
      } else {
        getScreenshot("/tmp/waterbird_" + id + "_screenshot.png", function(path) {
          debug("responding with screenshot");
          res.sendFile(path, function(err) {
            if(err) {
              next(err);
            } else {
              fs.unlink(path);
            }
          });

        });
      }
    });
    api.get(base_url + "/:type", async function(req, res) {
      let type = req.params.type;
      debug(type);
      let msg = {
        'id': 'controller',
        'type': type
      };
      switch(type) {
        case 'count':
          msg.value = controller.count();
          break;
        case 'success':
          msg.value = controller.success();
          break;
        case 'failure':
          msg.value = controller.failure();
          break;
        case 'uptime':
          msg.value = controller.formatTime(controller.uptime());
          break;
        case 'botTime':
          msg.value = controller.formatTime(controller.botTime());
          break;
        case 'childCount':
          msg.value = controller.botCount();
          break;
        case 'stats':
          msg.value = {
            success: controller.success(),
            failure: controller.failure()
          };
      }
      res.json(msg);
    });
  }

  broadcast(msg) {
    if(typeof msg === 'object') {
      msg = JSON.stringify(msg);
    }
    //debug_broadcast("Broadcasting message: " + msg);
    this.wss.clients.forEach(function(client) {
      client.send(msg);
    });
    return this;
  }

  start() {
    debug("Starting API Server on port " + this.port);
    this.server = this.api.listen(this.port);
    debug("Server running on port " + this.port);
    return this;
  }

  stop() {
    debug("Stopping API Server on port " + this.port);
    this.server.close();
    return this;
  }

}

module.exports = wbAPI;
